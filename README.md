# toy-robot-simulator

## Running
To run the project locally;
* Install: `npm i`
* Build: `npm run build`
* Start: `npm run start place 0 0 north`. This would place the robot at position 0, 0 facing the north direction

## Testing
* Unit Test: `npm run test`
   * This would run unit tests for 'Robot' and 'RobotValidations' class
* Add more unit tests to test edge cases if necessary

## Limitations
Most of my testing was done through unit testing. As such, i haven't done much plumbing work for cli interactions. I did not want to spend too much time on this part.
* Limited CLI interactions. 
* Limited input sanitization.