import { Config } from "jest";

const jestConfig: Config = {
  preset: "ts-jest",
  testEnvironment: "node",
  testMatch: ["**/tests/**/*.spec.ts"],
  maxWorkers: 1,
  collectCoverage: true,
  coverageReporters: ["text", "cobertura"],
};

export default jestConfig;
