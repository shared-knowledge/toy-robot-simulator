import { Robot } from "../src/model/Robot";
import { RobotDirection } from "../src/model/RobotDirection";

describe("Robot", () => {
  let robot;

  beforeEach(() => {
    robot = new Robot();
  });

  it("should be initialized with default values", () => {
    expect(robot.isPlaced).toBe(false);
    expect(robot.report()).toBe(
      "Current Robot position: 0, 0, in direction north",
    );
  });

  it("should be placed at the specified position and direction", () => {
    robot.place(1, 2, RobotDirection.EAST);

    expect(robot.isPlaced).toBe(true);
    expect(robot.report()).toBe(
      "Current Robot position: 1, 2, in direction east",
    );
  });

  it("should not allow moving before being placed", () => {
    expect(() => robot.move()).toThrow("Robot has not been placed yet.");
  });

  it("should turn left correctly", () => {
    robot.place(0, 0, RobotDirection.NORTH);
    robot.left();

    expect(robot.report()).toBe(
      "Current Robot position: 0, 0, in direction west",
    );
  });

  it("should turn right correctly", () => {
    robot.place(0, 0, RobotDirection.NORTH);
    robot.right();

    expect(robot.report()).toBe(
      "Current Robot position: 0, 0, in direction east",
    );
  });

  it("should move to the correct position based on the current direction - move", () => {
    robot.place(0, 0, RobotDirection.NORTH);
    robot.move();

    expect(robot.report()).toBe(
      "Current Robot position: 0, 1, in direction north",
    );
  });

  it("should ignore invalid moves", () => {
    robot.place(4, 4, RobotDirection.NORTH);
    robot.move();

    expect(robot.report()).toBe(
      "Current Robot position: 4, 4, in direction north",
    );
  });
});
