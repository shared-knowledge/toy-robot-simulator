import { RobotValidations } from "../src/model/RobotValidations";
import { RobotDirection } from "../src/model/RobotDirection";

describe("RobotValidations", () => {
  describe("validateOnPlace", () => {
    it("should not throw an error for valid coordinates and direction", () => {
      expect(() =>
        RobotValidations.validateOnPlace(2, 3, RobotDirection.NORTH),
      ).not.toThrow();
    });

    it("should throw an error for out-of-bounds coordinates", () => {
      expect(() =>
        RobotValidations.validateOnPlace(5, 2, RobotDirection.EAST),
      ).toThrow("Intended coordinates (5,2) would be out of bounds.");
    });

    it("should throw an error for an invalid direction", () => {
      expect(() =>
        RobotValidations.validateOnPlace(
          1,
          1,
          "INVALID_DIRECTION" as RobotDirection,
        ),
      ).toThrow(
        "Invalid intended direction 'INVALID_DIRECTION'. Valid directions are north, east, south, west.",
      );
    });
  });

  describe("validateOnMove", () => {
    it("should not throw an error for valid coordinates", () => {
      expect(() => RobotValidations.validateOnMove(3, 4)).not.toThrow();
    });

    it("should throw an error for out-of-bounds coordinates", () => {
      expect(() => RobotValidations.validateOnMove(-1, 2)).toThrow(
        "Intended coordinates (-1,2) would be out of bounds.",
      );
    });
  });
});
