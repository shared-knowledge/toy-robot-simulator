export enum RobotDirection {
  NORTH = "north",
  SOUTH = "south",
  EAST = "east",
  WEST = "west",
}

export const RobotDirectionClockwise: RobotDirection[] = [
  RobotDirection.NORTH,
  RobotDirection.EAST,
  RobotDirection.SOUTH,
  RobotDirection.WEST,
];
export const RobotDirectionAntiClockwise: RobotDirection[] = [
  RobotDirection.NORTH,
  RobotDirection.WEST,
  RobotDirection.SOUTH,
  RobotDirection.EAST,
];
