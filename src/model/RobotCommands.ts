export enum RobotCommands {
  MOVE = "move",
  LEFT = "left",
  RIGHT = "right",
  REPORT = "report",
  PLACE = "place",
}
