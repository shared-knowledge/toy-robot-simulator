import { RobotDirection, RobotDirectionClockwise } from "./RobotDirection";

export class RobotValidations {
  private static readonly maxX: number = 4;
  private static readonly maxY: number = 4;
  private static readonly possibleDirections: RobotDirection[] =
    Object.values(RobotDirection);

  static validateOnPlace(
    x: number,
    y: number,
    intendedDirection: RobotDirection,
  ): void {
    this.validateCoordinates(x, y);
    this.validateDirection(intendedDirection);
  }

  static validateOnMove(x: number, y: number): void {
    this.validateCoordinates(x, y);
  }

  private static validateCoordinates(x: number, y: number): void {
    if (x < 0 || x > this.maxX || y < 0 || y > this.maxY) {
      throw new Error(
        `Intended coordinates (${x},${y}) would be out of bounds.`,
      );
    }
  }

  private static validateDirection(intendedDirection: RobotDirection): void {
    if (!this.possibleDirections.includes(intendedDirection)) {
      throw new Error(
        `Invalid intended direction '${intendedDirection}'. Valid directions are ${RobotDirectionClockwise.join(
          ", ",
        )}.`,
      );
    }
  }
}
