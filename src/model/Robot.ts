import {
  RobotDirection,
  RobotDirectionAntiClockwise,
  RobotDirectionClockwise,
} from "./RobotDirection";
import { RobotValidations } from "./RobotValidations";

export class Robot {
  private x: number = 0;
  private y: number = 0;
  private currentDirection: RobotDirection = RobotDirection.NORTH;

  private _isPlaced: boolean = false;
  public get isPlaced(): boolean {
    return this._isPlaced;
  }

  public place(x: number, y: number, intendedDirection: RobotDirection): void {
    RobotValidations.validateOnPlace(x, y, intendedDirection);

    this.x = x;
    this.y = y;
    this.currentDirection = intendedDirection;
    this._isPlaced = true;
  }

  public move(): void {
    if (!this._isPlaced) {
      throw new Error("Robot has not been placed yet.");
    }

    try {
      switch (this.currentDirection) {
        case RobotDirection.NORTH: {
          RobotValidations.validateOnMove(this.x, this.y + 1);
          this.y++;
          return;
        }
        case RobotDirection.SOUTH: {
          RobotValidations.validateOnMove(this.x, this.y - 1);
          this.y--;
          return;
        }
        case RobotDirection.EAST: {
          RobotValidations.validateOnMove(this.x + 1, this.y);
          this.x++;
          return;
        }
        case RobotDirection.WEST: {
          RobotValidations.validateOnMove(this.x - 1, this.y);
          this.x--;
          return;
        }
      }
    } catch (err: any) {
      const errorMessage = (err as Error).message;
      console.error(errorMessage);
    }
  }

  public left(): void {
    this.currentDirection =
      RobotDirectionAntiClockwise[
        (RobotDirectionAntiClockwise.indexOf(this.currentDirection) + 1) %
          RobotDirectionAntiClockwise.length
      ];
  }

  public right(): void {
    this.currentDirection =
      RobotDirectionClockwise[
        (RobotDirectionClockwise.indexOf(this.currentDirection) + 1) %
          RobotDirectionClockwise.length
      ];
  }

  public report(): string {
    return `Current Robot position: ${this.x}, ${this.y}, in direction ${this.currentDirection}`;
  }
}
