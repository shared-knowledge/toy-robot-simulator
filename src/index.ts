import * as readline from "node:readline/promises";
import { CommandHandler } from "./handlers";

// cli();

// async function cli() {
//   const args = process.argv.slice(2);

//   const handler = new CommandHandler();
//   handler.handle(args);
// }

const handler = new CommandHandler();
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.on("line", (input) => {
  handler.handle(input.trim().split(","));
});
