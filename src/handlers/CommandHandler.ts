import { Robot, RobotCommands, RobotDirection } from "../model";

export class CommandHandler {
  private readonly robot: Robot;

  constructor() {
    this.robot = new Robot();
  }

  public handle(args: string[]): void {
    try {
      const [cmd, x, y, intendedDirection] = args;

      if (cmd.toLowerCase() === RobotCommands.PLACE) {
        this.robot.place(
          parseInt(x),
          parseInt(y),
          intendedDirection as RobotDirection,
        );
      } else if (this.robot.isPlaced) {
        switch (cmd) {
          case RobotCommands.MOVE:
            this.robot.move();
            break;
          case RobotCommands.LEFT:
            this.robot.left();
            break;
          case RobotCommands.RIGHT:
            this.robot.right();
            break;
          case RobotCommands.REPORT:
            console.log(this.robot.report());
            break;
        }
      }
    } catch (error) {
      throw new Error(
        `Error processing command: Error [${JSON.stringify(error)}]`,
      );
    }
  }
}
